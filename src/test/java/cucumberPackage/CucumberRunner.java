package cucumberPackage;


import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.junit.runner.RunWith;
 

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(	
		plugin = {"pretty", "html:build/reports/tests/cucumber-html-report", "json:build/reports/tests/cucumber-json-report"},
		features = {"src/test/java/cucumberPackage"},
		tags = {""},
		snippets = SnippetType.CAMELCASE,
		monochrome = false
		)
public class CucumberRunner {
	
}
